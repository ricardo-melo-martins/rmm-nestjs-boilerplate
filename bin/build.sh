#/bin/bash

# Ricardo Melo Martins
# 
# Referencias
# https://docs.docker.com/engine/reference/commandline/build/

echo "docker build"

docker build \
    -f .docker/Dockerfile \
    --build-arg POSTGRES_VERSION=13.2-alpine \
    --build-arg TZ=UTC \
    --target database \
    -t rmm_postgres13 .

docker image ls