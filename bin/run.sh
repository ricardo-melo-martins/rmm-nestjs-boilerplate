#/bin/bash

# Ricardo Melo Martins
# 
# Referencias
# https://docs.docker.com/engine/reference/commandline/run/

echo "docker run"

docker run -d \
     --env-file=config/.env.dev \
     --name db_postgres13 \
     -p 5432:5432 \
     -h RMM_database \
     rmm_postgres13

    #  -v /data:/var/lib/postgresql/data \