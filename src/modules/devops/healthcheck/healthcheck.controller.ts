import { Controller, Get } from '@nestjs/common'

@Controller('/devops/healthcheck')
export class HealthcheckController {
  @Get()
  public async ack(): Promise<string> {
    return new Date().toString()
  }
}
