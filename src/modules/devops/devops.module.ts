import { Module } from '@nestjs/common'
import { HealthcheckController } from '../devops/healthcheck/healthcheck.controller'

@Module({
  controllers: [HealthcheckController]
})
export class DevopsModule {}
