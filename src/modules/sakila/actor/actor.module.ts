import { Module } from '@nestjs/common'
import { ActorService } from './services/actor.service'
import { ActorController } from './controllers/actor.controller'
import { Actor } from './entities/actor.entity'
import { TypeOrmModule } from '@nestjs/typeorm'

@Module({
  imports: [TypeOrmModule.forFeature([Actor])],
  exports: [TypeOrmModule],
  controllers: [ActorController],
  providers: [ActorService]
})
export class ActorModule {}
