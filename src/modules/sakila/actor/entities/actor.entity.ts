import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Actor extends BaseEntity {
  @PrimaryGeneratedColumn()
  actor_id: number
  @Column({ nullable: false, type: 'varchar', length: 45 })
  first_name: string
  @Column({ nullable: false, type: 'varchar', length: 45 })
  last_name: string
  @Column({ nullable: false, type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  last_update: string
}
