import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { ActorRepository } from '../repositories/actor.repository'

import { CreateActorDto } from '../dto/create-actor.dto'
import { UpdateActorDto } from '../dto/update-actor.dto'

import { Actor } from '../entities/actor.entity'

@Injectable()
export class ActorService {
  constructor(@InjectRepository(Actor) private actorRepository: ActorRepository) {}

  /**
   * Find all actors
   *
   * @returns Promise<Actor[]>
   */
  findAll(): Promise<Actor[]> {
    return this.actorRepository.find()
  }

  /**
   * Create actor
   *
   * @param createActorDto
   * @returns
   */
  async create(createActorDto: CreateActorDto): Promise<CreateActorDto> {
    return await this.actorRepository.save(createActorDto)
  }

  /**
   * Find One actor by ID
   *
   * @param id
   * @returns
   */
  async findOne(id: number): Promise<Actor> {
    const actor = await this.actorRepository.findOne({
      where: { actor_id: id }
    })

    if (!actor) {
      throw new NotFoundException('Actor Not Found!')
    }

    return actor
  }

  /**
   * Update actor
   *
   * @param id
   * @param updateActorDto
   * @returns Promise<Actor>
   */
  async update(id: number, updateActorDto: UpdateActorDto): Promise<Actor> {
    const actor = await this.findOne(id)

    if (!actor) {
      throw new NotFoundException('Actor Not Found!')
    }

    await this.actorRepository.update(id, updateActorDto)

    return await this.findOne(id)
  }

  /**
   * Remove actor
   *
   * @param id
   */
  async remove(id: number) {
    const result = await this.actorRepository.delete({ actor_id: id })
    if (result.affected === 0) {
      throw new NotFoundException('Actor Not Found!')
    }
  }
}
