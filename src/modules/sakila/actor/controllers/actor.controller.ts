import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { ActorService } from '../services/actor.service'
import { CreateActorDto } from '../dto/create-actor.dto'
import { UpdateActorDto } from '../dto/update-actor.dto'
import { Actor } from '../entities/actor.entity'

@Controller('actor')
export class ActorController {
  constructor(private readonly actorService: ActorService) {}

  @Get()
  findAll(): Promise<Actor[]> {
    return this.actorService.findAll()
  }

  @Post()
  create(@Body() createActorDto: CreateActorDto): Promise<CreateActorDto> {
    return this.actorService.create(createActorDto)
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.actorService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateActorDto: UpdateActorDto) {
    return this.actorService.update(+id, updateActorDto)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.actorService.remove(+id)
  }
}
