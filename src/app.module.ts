import { Module } from '@nestjs/common'
import { PublicModule } from './modules/public/public.module'
import { DevopsModule } from './modules/devops/devops.module'
import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ActorModule } from './modules/sakila/actor/actor.module'

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5433,
  username: 'postgres',
  password: 'sakila',
  database: 'sakila',
  autoLoadEntities: true,
  synchronize: false
}
@Module({
  imports: [TypeOrmModule.forRoot(typeOrmConfig), PublicModule, DevopsModule, ActorModule],
  controllers: [],
  providers: []
})
export class AppModule {}
